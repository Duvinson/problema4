/*Requerimiento: No se necesita ningun requerimiento por parte del usuario, solo se necesita
  generar numeros aleatorios para llenar la cadena

  Garantiza : Que una cadena de caracteres numéricos, la convierta a un número entero. */

#include <iostream>
#include <time.h>

using namespace std;

int funcionNumero(char *punteroCadena);//prototipo de funcion que me recibe un puntero y me retorna un entero

int main()
{
    srand(time(NULL));

    int aux, i;
    char Cadena[5] = {};//declaracion de una cadena de 5 posiciones


    for(i=0; i<5; i++){//ciclo que me recorre cada posicion de la cadena
        Cadena[i] = 48 + rand() %(8);//instruccion que me llena la cadena con numeros aleatorios segun la tabla ascii
        cout << Cadena[i]<< endl; //imprime la cadena
    }

    aux = funcionNumero(Cadena);//llamado a la funcion que me devuleve los numeros que hay en la cadena convertidos a enteros.

    cout << aux << endl;//imprime el numero entero
    return 0;
}

int funcionNumero(char *punteroCadena){//implementacion de la funcion

    int i, aux=0 ,val=0;
    for(i=0; i<5; i++){ //ciclo que me recorre todas las posiciones de la cadena por medio de punteros
        aux = *(punteroCadena+i)-48;//instruccion para convertir lo que hay dentro de la posicion de la cadena y se le resta 48 para que se convierta en numero
        val += aux;//se le va asignando a val lo que hay dentro de aux y se va sumando
        val *= 10;//Se multiplica por 10 para correr el numero decenas, centenas...
    }

    return val/10;//por ultimo se retorna el numero entero pero antes se divide por 10 por que la ultima instruccion sera realizada una vez mas
}
